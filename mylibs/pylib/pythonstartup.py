#!/usr/bin/env python
"""Some helpful interactive shell functions.
This file is currently set to the PYTHONSTARTUP environment variable.
"""

from myutils import c
from myutils import cd
from myutils import cdd
from myutils import cdh
from myutils import copy_templates
from myutils import ENV
from myutils import get_file_contents
from myutils import ls
from myutils import pretty_print
from myutils import print_intro
from myutils import pwd
from myutils import q
